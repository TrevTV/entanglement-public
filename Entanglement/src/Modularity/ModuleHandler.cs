﻿using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using System.Linq;

using MelonLoader;

using Entanglement.Extensions;

namespace Entanglement.Modularity {
    public static class ModuleHandler {
        public static readonly List<EntanglementModule> loadedModules = new List<EntanglementModule>();

        public static string modulePath = Directory.GetCurrentDirectory().Replace('\\', '/') + "/UserData/Entanglement/Modules/";
        public static string moduleDependencyPath = modulePath + "Dependencies/";

        // Should be called using reflection
        public static void SetupModule(Assembly moduleAssembly) {
            if (moduleAssembly != null) {
                var moduleInfo = moduleAssembly.GetCustomAttribute<EntanglementModuleInfo>();
            
                // *sigh* reflection is ugly at times ngl
                if (moduleInfo != null) {
                    if (moduleInfo.moduleType != null) {
                        PrintSpacer(moduleInfo);

                        if (typeof(EntanglementModule).IsAssignableFrom(moduleInfo.moduleType) && !moduleInfo.moduleType.IsAbstract) {
                            EntanglementModule module = Activator.CreateInstance(moduleInfo.moduleType) as EntanglementModule;

                            if (module != null) {
                                loadedModules.Add(module);
                                module.OnModuleLoaded();
                            }
                        }
                    }
                }
            }
        }

        // Ugly but idc enough to make it look pretty
        internal static void PrintSpacer(EntanglementModuleInfo moduleInfo) {
            MelonLogger.Msg(ConsoleColor.Magenta, "--==== Entanglement Module ====--");

            MelonLogger.Msg($"{moduleInfo.name} - v{moduleInfo.version}");

            if (!string.IsNullOrEmpty(moduleInfo.abbreviation))
                MelonLogger.Msg($"aka [{moduleInfo.abbreviation}]");

            MelonLogger.Msg($"by {moduleInfo.author}");

            MelonLogger.Msg(ConsoleColor.Magenta, "--=============================--");
        }

        //
        // MelonLoader wrappers
        //
        public static void Update() {
            foreach (var module in loadedModules)
                module.Update();
        }

        public static void FixedUpdate() {
            foreach (var module in loadedModules)
                module.FixedUpdate();
        }

        public static void LateUpdate() {
            foreach (var module in loadedModules)
                module.LateUpdate();
        }

        public static void OnSceneWasInitialized(int buildIndex, string sceneName) {
            foreach (var module in loadedModules)
                module.OnSceneWasInitialized(buildIndex, sceneName);
        }

        public static void OnLoadingScreen() {
            foreach (var module in loadedModules)
                module.OnLoadingScreen();
        }

        public static void OnApplicationQuit() {
            foreach (var module in loadedModules)
                module.OnApplicationQuit();
        }
    }
}
