﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;

using UnityEngine;

using Entanglement.Extensions;

using MelonLoader;

namespace Entanglement.Data {
    public static class EmbeddedResource {
        public static void ListResourcesFromAssembly(Assembly assembly) {
            foreach (string resource in assembly.GetManifestResourceNames())
                MelonLogger.Msg(ConsoleColor.DarkCyan, "Resource: " + resource);
        }

        public static byte[] LoadFromAssembly(Assembly assembly, string name) {
            string[] manifestResources = assembly.GetManifestResourceNames();

            if (manifestResources.Contains(name)) {
                MelonLogger.Msg(ConsoleColor.DarkCyan, $"Loading embedded resource data {name}...");
                using (Stream str = assembly.GetManifestResourceStream(name))
                using (MemoryStream memoryStream = new MemoryStream()) {
                    str.CopyTo(memoryStream);
                    MelonLogger.Msg(ConsoleColor.DarkCyan, "Done!");
                    return memoryStream.ToArray();
                }
            }

            return null;
        }
    }
}
