﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;

using UnityEngine;

using Entanglement.Extensions;

using MelonLoader;

namespace Entanglement.Data {
    public static class EmebeddedAssetBundle {
        public static AssetBundle LoadFromAssembly(Assembly assembly, string name) {
            string[] manifestResources = assembly.GetManifestResourceNames();

            if (manifestResources.Contains(name)) {
                MelonLogger.Msg(ConsoleColor.DarkCyan, $"Loading embedded bundle data {name}...");

                byte[] bytes;
                using (Stream str = assembly.GetManifestResourceStream(name))
                using (MemoryStream memoryStream = new MemoryStream()) {
                    str.CopyTo(memoryStream);
                    bytes = memoryStream.ToArray();
                }

                MelonLogger.Msg(ConsoleColor.DarkCyan, $"Loading bundle from data {name}, please be patient...");
                var temp = AssetBundle.LoadFromMemory(bytes);     
                MelonLogger.Msg(ConsoleColor.DarkCyan, $"Done!");
                return temp;
            }

            return null;
        }
    }
}
