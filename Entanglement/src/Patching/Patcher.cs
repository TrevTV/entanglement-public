﻿using System.Reflection;

using HarmonyLib;

using Entanglement.Compat;

namespace Entanglement.Patching {
    public static class Patcher {
        public static HarmonyLib.Harmony harmonyInstance;

        public static void Initialize() {
            harmonyInstance = new HarmonyLib.Harmony("Entanglement.Harmony");
            harmonyInstance.PatchAll();

            OptionalAssemblyPatch.AttemptPatches();
        }

        public static void Patch(MethodBase method, HarmonyMethod prefix = null, HarmonyMethod postfix = null) => harmonyInstance.Patch(method, prefix, postfix);
    }
}
