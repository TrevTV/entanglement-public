﻿namespace Entanglement.Patching
{
    public static class PatchCheck {
        internal static int count = 0;

        public static bool IsValid() {
            // Prevents running twice
            if (count <= 0) {
                count++; return false;
            }
            count = 0;
            return true;
        }

        public static bool InverseValid()
        {
            // Prevents running twice
            if (count > 0) {
                count = 0; return false;
            }
            count++;
            return true;
        }
    }
}
