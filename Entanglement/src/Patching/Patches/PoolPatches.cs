﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entanglement.Objects;

using StressLevelZero.Pool;

using HarmonyLib;

using UnityEngine;

namespace Entanglement.Patching {

    public static class Pool_Settings {
        public static List<Poolee> GetAllPoolees(this Pool pool) {
            List<Poolee> poolees;
            if (!ObjectSync.poolPairs.TryGetValue(pool, out poolees)) {
                poolees = new List<Poolee>();
                ObjectSync.poolPairs.Add(pool, poolees);
            }
            return poolees;
        }

        public static float GetRelativeSpawnTime(this Poolee poolee) {
            if (!poolee.pool) return -1f;
            float timeSpawned = poolee.gameObject.activeInHierarchy ? poolee.timeSpawned : 0f;
            return (float)poolee.pool._timeOfLastSpawn - timeSpawned;
        }

        public static Poolee GetAccuratePoolee(this Pool pool, int index, float relativeTime = -1f) {
            List<Poolee> poolees = pool.GetAllPoolees();
            if (poolees.Count <= 0) return null;

            Poolee toReturn = null;

            if (relativeTime < 0f)
                return poolees[Math.Min(index, poolees.Count() - 1)];
            else {
                int closestIndex = -1;
                float closestTime = -1f;
                for (int thisIndex = 0; thisIndex < poolees.Count(); thisIndex++) {
                    Poolee thisPoolee = poolees[thisIndex];
                    float thisTime = thisPoolee.GetRelativeSpawnTime();

                    if (Mathf.Abs(thisTime - relativeTime) > Mathf.Abs(closestTime - relativeTime))
                        continue;

                    if (Math.Abs(thisIndex - index) > Math.Abs(closestIndex - index))
                        continue;

                    closestIndex = thisIndex;
                    closestTime = thisTime;
                    toReturn = thisPoolee;
                }
            }

            return toReturn;
        }
    }

    [HarmonyPatch(typeof(Pool), "InstantiatePoolee")]
    public static class InstantiatePatch {
        public static void Postfix(Pool __instance, Poolee __result, Vector3 position, Quaternion rotation) {
            try {
                if (__instance._pooledObjects.Contains(__result)) {
                    List<Poolee> poolees;

                    if (!ObjectSync.poolPairs.TryGetValue(__instance, out poolees))
                    {
                        poolees = new List<Poolee>();
                        if (ObjectSync.poolPairs.ContainsKey(__instance))
                            ObjectSync.poolPairs[__instance] = poolees;
                        else
                            ObjectSync.poolPairs.Add(__instance, poolees);
                    }

                    if (!poolees.Contains(__result)) poolees.Add(__result);
                }
            } catch { }
        }
    }
}
