﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;

using MelonLoader;

using Entanglement.Representation;
using Entanglement.Network;
using Entanglement.Data;
using Entanglement.Patching;
using Entanglement.UI;
using Entanglement.Objects;
using Entanglement.Compat;
using Entanglement.Extensions;
using Entanglement.Modularity;

using StressLevelZero.Pool;

using UnityEngine;

// This mod is not a rewrite of the multiplayer mod!
// It is another MP mod made by an ex developer of the MP mod that was unsatisfied with the original mod's codebase
// There is no shared code between the two projects and any similar code is accidental / coincidental

namespace Entanglement {
    // We can compare with peers to see if they are on a supported version
    public struct EntanglementVersion {
        public const byte versionMajor = 0;
        public const byte versionMinor = 0;
        public const short versionPatch = 5;

        // Patches don't matter too much when supporting old versions
        // Although we don't support anything newer than the current version, just in case
        public const byte minVersionMajorSupported = 0;
        public const byte minVersionMinorSupported = 0;
    }

    public class EntanglementMod : MelonMod {
        public static byte? sceneChange = null;
        public static Assembly entanglementAssembly;

        public override void OnApplicationStart() {
            entanglementAssembly = Assembly.GetExecutingAssembly();

            MelonLogger.Msg($"Current Entanglement version is {EntanglementVersion.versionMajor}.{EntanglementVersion.versionMinor}.{EntanglementVersion.versionPatch}");
            MelonLogger.Msg($"Minimum supported Entanglement version is {EntanglementVersion.minVersionMajorSupported}.{EntanglementVersion.minVersionMinorSupported}.*");

            PersistentData.Initialize();
            GameSDK.LoadGameSDK();

#if DEBUG
            MelonLogger.Msg(ConsoleColor.Blue, "Entanglement Debug Build!");
#endif

            Patcher.Initialize();

            NetworkMessage.RegisterHandlersFromAssembly(entanglementAssembly);

            MelonLogger.Msg(ConsoleColor.Magenta, "If your game freezes past this point, please install Discord! \n This mod runs through Discord and does not support the browser!");

            DiscordIntegration.Initialize();

            Client.StartClient();

            PlayerRepresentation.LoadBundle();
            LoadingScreen.LoadBundle();

            ObjectSync.Initialize();

            EntanglementUI.CreateUI();

            BanList.PullFromFile();

            // TODO: Remove this upon full release
            MelonLogger.Msg(ConsoleColor.DarkYellow, "Welcome to the Entanglement pre-release!");

        }

        public override void OnUpdate() {
            ModuleHandler.Update();

#if DEBUG
            if (Input.GetKeyDown(KeyCode.S))
                Server.StartServer();

            if (Input.GetKeyDown(KeyCode.K))
                Server.instance?.Shutdown();

            if (Input.GetKeyDown(KeyCode.R))
                PlayerRepresentation.debugRepresentation = new PlayerRepresentation("Dummy", 0);
#endif
            StatsUI.UpdateUI();
            PlayerRepresentation.SyncPlayerReps();
            DataTransaction.Process();

            DiscordIntegration.Update();
        }

        public override void OnFixedUpdate() {
            ModuleHandler.FixedUpdate();

            // Updates the VRIK of all the players
            PlayerRepresentation.UpdatePlayerReps();
        }

        public override void OnLateUpdate() {
            ModuleHandler.LateUpdate();

            Client.instance?.Tick();
            Server.instance?.Tick();

            // This will flush discords callbacks
            DiscordIntegration.Flush();
        }

        public override void OnSceneWasInitialized(int buildIndex, string sceneName) {
            ModuleHandler.OnSceneWasInitialized(buildIndex, sceneName);

            SpawnableData.GetData();

            SceneExtensions.CacheObjects();

            PlayerScripts.GetPlayerScripts();

            PlayerRepresentation.GetPlayerTransforms();

            foreach (var rep in PlayerRepresentation.representations.Values)
                rep.RecreateRepresentations();

            Client.instance.currentScene = (byte)buildIndex;
            sceneChange = (byte)buildIndex;
        }

        public override void BONEWORKS_OnLoadingScreen() {
            ModuleHandler.OnLoadingScreen();

            LoadingScreen.OverrideScreen();

            ObjectSync.OnCleanup();
            ObjectSync.poolPairs.Clear();

#if DEBUG
            PlayerRepresentation.debugRepresentation = null;
#endif
        }

        public override void OnApplicationQuit() {
            ModuleHandler.OnApplicationQuit();

            Node.activeNode.Shutdown();
            DiscordIntegration.Shutdown();
        }
    }
}
