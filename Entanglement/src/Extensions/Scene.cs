﻿using System.Collections.Generic;
using System.Linq;

using UnityEngine.SceneManagement;
using UnityEngine;

using Il2List = Il2CppSystem.Collections.Generic.List<UnityEngine.GameObject>;

namespace Entanglement.Extensions {
    public static class SceneExtensions {

        internal static List<GameObject> rootObjects = new List<GameObject>(short.MaxValue);
        internal static Il2List memoryObjects = new Il2List(short.MaxValue);

        public static Dictionary<string, GameObject> cachedRootObjects = null;

        internal static Scene[] GetLoadedScenes() {
            int countLoaded = SceneManager.sceneCount;
            Scene[] loadedScenes = new Scene[countLoaded];

            for (int i = 0; i < countLoaded; i++) {
                Scene scene = SceneManager.GetSceneAt(i);
                loadedScenes[i] = scene;
            }

            return loadedScenes;
        }

        internal static List<GameObject> GetRootObjects() {
            rootObjects.Clear();
            Scene[] loadedScenes = GetLoadedScenes();
            for (int i = 0; i < loadedScenes.Length; i++) {
                Scene scene = loadedScenes[i];
                scene.GetRootGameObjects(memoryObjects);
                rootObjects.AddRange(memoryObjects.ToArray());
            }
            return rootObjects;
        }

        internal static void CacheObjects() {
            List<GameObject> rootObjects = GetRootObjects();
            cachedRootObjects = new Dictionary<string, GameObject>(rootObjects.Count);
            for (int i = 0; i < rootObjects.Count; i++) {
                GameObject go = rootObjects[i];
                if (cachedRootObjects.ContainsKey(go.name)) continue;
                cachedRootObjects.Add(go.name, go);
            }
        }

        internal static GameObject GetRootObjectCached(string name) {
            if (cachedRootObjects == null) CacheObjects();
            if (cachedRootObjects.ContainsKey(name)) return cachedRootObjects[name];

            GameObject go = GameObject.Find(name);
            if (go) cachedRootObjects.Add(go.name, go);
            return go;
        }
    }
}
