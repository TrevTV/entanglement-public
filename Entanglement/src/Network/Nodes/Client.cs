﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MelonLoader;

using Entanglement.Compat.Playermodels;
using Entanglement.Representation;
using Entanglement.Objects;

using Discord;

namespace Entanglement.Network {
    public class Client : Node {
        // There can only be one client, otherwise things will break
        public static Client instance = null;

        public static void StartClient()
        {
            if (instance != null)
                throw new Exception("Can't create another client instance!");

            MelonLogger.Msg($"Started client!");
            activeNode = instance = new Client();
        }

        //
        // Actual functionality
        //

        public User hostUser;

        public byte currentScene = 0;

        private Client() {
            DiscordIntegration.activityManager.OnActivityJoin += (secret) => DiscordIntegration.lobbyManager.ConnectLobbyWithActivitySecret(secret, DiscordJoinLobby);
        }

        public void DiscordJoinLobby(Result result, ref Lobby lobby) {
            if (DiscordIntegration.hasLobby) {
                MelonLogger.Error("You are already in a lobby!");
                return;
            }

            if (result != Result.Ok)
                return;

            DiscordIntegration.lobby = lobby;
            ConnectToDiscordServer();

            DiscordIntegration.userManager.GetUser(lobby.OwnerId, OnDiscordHostUserFetched);

            DiscordIntegration.lobbyManager.OnNetworkMessage += OnDiscordMessageRecieved;
            DiscordIntegration.lobbyManager.OnMemberConnect += OnDiscordUserJoined;
            DiscordIntegration.lobbyManager.OnMemberDisconnect += OnDiscordUserLeft;

            IEnumerable<User> users = DiscordIntegration.lobbyManager.GetMemberUsers(lobby.Id);

            foreach (User user in users)
                if (user.Id != DiscordIntegration.currentUser.Id && user.Id != lobby.OwnerId)
                    CreatePlayerRep(user.Id);

            DiscordIntegration.activity.Party = new ActivityParty()
            {
                Id = lobby.Id.ToString(),
                Size = new PartySize() { CurrentSize = users.Count(), MaxSize = (int)lobby.Capacity }
            };

            DiscordIntegration.activity.Secrets = new ActivitySecrets()
            {
                Join = DiscordIntegration.lobbyManager.GetLobbyActivitySecret(lobby.Id)
            };

            DiscordIntegration.activity.Details = "";
            DiscordIntegration.activity.State = "Playing in a server";

            DiscordIntegration.activity.Assets = new ActivityAssets() {
                LargeImage = "entanglement"
            };

            DiscordIntegration.activity.Instance = true;

            DiscordIntegration.activityManager.UpdateActivity(DiscordIntegration.activity, (x) => { });

            ObjectSync.OnCleanup();
        }

        public void OnDiscordHostUserFetched(Result result, ref User user) {
            PlayerRepresentation.representations.Add(user.Id, new PlayerRepresentation(user.Username, user.Id));
            userDatas.Add(user.Id, user);

            hostUser = user;
            MelonLogger.Msg($"Joined {hostUser.Username}'s server!");

            // Test our connection by sending our connection message
            ConnectionMessageData connectionData = new ConnectionMessageData();
            connectionData.packedVersion = BitConverter.ToUInt16(new byte[] { EntanglementVersion.versionMajor, EntanglementVersion.versionMinor }, 0);

            NetworkMessage conMsg = NetworkMessage.CreateMessage((byte)BuiltInMessageType.Connection, connectionData);
            SendMessage(hostUser.Id, NetworkChannel.Reliable, conMsg.GetBytes());

            DiscordIntegration.RegisterUser(hostUser.Id, 0);
        }

        public void DisconnectFromServer() {
            DiscordIntegration.lobbyManager.DisconnectLobby(DiscordIntegration.lobby.Id, (res) => { });
            DiscordIntegration.lobby = new Lobby(); // Clear the lobby
            DiscordIntegration.DefaultRichPresence();
            CleanData();
        }

        public override void BroadcastMessage(NetworkChannel channel, byte[] data) => SendMessage(hostUser.Id, channel, data);

        // Client.Shutdown is ran on closing the game
        public override void Shutdown() {
            DisconnectFromServer();
        }
    }
}
