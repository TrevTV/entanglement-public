﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entanglement.Network
{
    public static class Net
    {
        // Tells the register method to not register this automatically
        [System.AttributeUsage(System.AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
        public class NoAutoRegister : Attribute { }
    }
}
