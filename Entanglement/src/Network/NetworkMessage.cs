﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;

using Entanglement.Extensions;

using MelonLoader;

namespace Entanglement.Network
{
    public enum NetworkChannel : byte {
        Reliable    = 0,
        Unreliable  = 1,
        Attack      = 2,
        Object      = 3,
        Transaction = 4
    }

    public class NetworkMessage {
        public byte messageType;
        public byte[] messageData;

        public byte[] GetBytes()
        {
            byte[] bytes = new byte[1 + messageData.Length];

            bytes[0] = messageType;

            for (int b = 1; b < bytes.Length; b++)
                bytes[b] = messageData[b - 1]; // Let's hope this doesn't break

            return bytes;
        }



        //
        // Static stuff
        //

        // Handlers are created up front, they're not static
        public static void RegisterHandlersFromAssembly(Assembly targetAssembly) {
            if (targetAssembly == null) throw new NullReferenceException("Can't register from a null assembly!");

            MelonLogger.Msg($"Populating MessageHandler list from {targetAssembly.GetName().Name}!");

            // I am aware LINQ is kinda gross but this is works!
            targetAssembly.GetTypes()
                .Where(type => typeof(NetworkMessageHandler).IsAssignableFrom(type) && !type.IsAbstract)
                .Where(type => type.GetCustomAttribute<Net.NoAutoRegister>() == null)
                .ForEach(type => {
                    try
                    {
                        RegisterHandler(type);
                    }
                    catch (Exception e)
                    {
                        MelonLogger.Error(e.Message);
                    }
                });
        }

        public static void RegisterHandler<T>() where T : NetworkMessageHandler => RegisterHandler(typeof(T));

        protected static void RegisterHandler(Type type)
        {
            NetworkMessageHandler handler = Activator.CreateInstance(type) as NetworkMessageHandler;

            if (handler.MessageIndex == null)
            {
                MelonLogger.Warning($"Didn't register {type.Name} because its message index was null!");
            }
            else
            {
                byte index = handler.MessageIndex.Value;

                if (handlers[index] != null) throw new Exception($"{type.Name} has the same index as {handlers[index].GetType().Name}, we can't replace handlers!");

                MelonLogger.Msg($"Registered {type.Name}");
                handlers[index] = handler;
            }
        } 

        public static NetworkMessage CreateMessage(byte type, NetworkMessageData data) {
            try {
                return handlers[type].CreateMessage(data);
            }
            catch (Exception e) {
                MelonLogger.Error($"Failed creating network message with reason: {e.Message}\nTrace:{e.StackTrace}");
            }

            return null;
        }

        public static void HandleMessage(NetworkMessage message, long sender) {
            try {
                handlers[message.messageType].HandleMessage(message, sender);
            }
            catch (Exception e) {
                MelonLogger.Error($"Failed handling network message with reason: {e.Message}\nTrace:{e.StackTrace}");
            }
        }

        public static readonly NetworkMessageHandler[] handlers = new NetworkMessageHandler[byte.MaxValue];
    }
}
