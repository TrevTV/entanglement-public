﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

using Entanglement.Data;
using Entanglement.Representation;
using Entanglement.Extensions;

using StressLevelZero;

namespace Entanglement.Network
{
    public class PlayerRepSyncHandler : NetworkMessageHandler {
        public override byte? MessageIndex => BuiltInMessageType.PlayerRepSync;

        public override NetworkMessage CreateMessage(NetworkMessageData data) {
            if (!(data is PlayerRepSyncData) || data == null)
                throw new Exception("Provided connection data was not of type PlayerRepSyncData or was null!");

            NetworkMessage message = new NetworkMessage();
            PlayerRepSyncData repData = data as PlayerRepSyncData;

            message.messageType = (byte)BuiltInMessageType.PlayerRepSync;

            List<byte> rawBytes = new List<byte>();

            rawBytes.AddRange(BitConverter.GetBytes(DiscordIntegration.GetShortId(repData.userId)));
            rawBytes.Add(Convert.ToByte(repData.isGrounded));

            rawBytes.AddRange(repData.rootPosition.GetBytes());

            for (int r = 0; r < repData.simplifiedTransforms.Length; r++)
                rawBytes.AddRange(repData.simplifiedTransforms[r].GetSmallBytes(repData.rootPosition));

            rawBytes.AddRange(repData.simplifiedLeftHand.GetBytes());
            rawBytes.AddRange(repData.simplifiedRightHand.GetBytes());

            message.messageData = rawBytes.ToArray();

            return message;
        }

        public override void HandleMessage(NetworkMessage message, long sender) {
            if (message.messageData.Length <= 0)
                throw new IndexOutOfRangeException();

            if (SceneLoader.loading)
                return;

            int index = 0;
            long userId = DiscordIntegration.GetLongId(BitConverter.ToUInt16(message.messageData, index));
            index += sizeof(ushort);

            if (PlayerRepresentation.representations.ContainsKey(userId)) {
                PlayerRepresentation rep = PlayerRepresentation.representations[userId];

                if (rep.repFord) {

                    bool isGrounded = Convert.ToBoolean(message.messageData[index]);
                    index += sizeof(byte);
                    rep.isGrounded = isGrounded;

                    List<byte> data = message.messageData.ToList();

                    Vector3 rootPosition = new Vector3();

                    rootPosition.x = BitConverter.ToSingle(message.messageData, index);
                    index += sizeof(float);
                    rootPosition.y = BitConverter.ToSingle(message.messageData, index);
                    index += sizeof(float);
                    rootPosition.z = BitConverter.ToSingle(message.messageData, index);
                    index += sizeof(float);

                    rep.repRoot.position = rootPosition;

                    for (int r = 0; r < rep.repTransforms.Length; r++)
                    {
                        SimplifiedTransform simpleTransform = SimplifiedTransform.FromSmallBytes(data.GetRange(index, SimplifiedTransform.size_small).ToArray(), rootPosition);
                        index += SimplifiedTransform.size_small;

                        if (rep.repTransforms[r])
                            simpleTransform.Apply(rep.repTransforms[r]);
                    }

                    SimplifiedHand simplifiedLeftHand = SimplifiedHand.FromBytes(data.GetRange(index, SimplifiedHand.size).ToArray());
                    index += SimplifiedHand.size;
                    SimplifiedHand simplifiedRightHand = SimplifiedHand.FromBytes(data.GetRange(index, SimplifiedHand.size).ToArray());

                    rep.UpdateFingers(Handedness.LEFT, simplifiedLeftHand);
                    rep.UpdateFingers(Handedness.RIGHT, simplifiedRightHand);

                    if (rep.repCanvasTransform) {
                        rep.repCanvasTransform.position = rep.repTransforms[0].position + Vector3.up * 0.4f;

                        if (Camera.current)
                            rep.repCanvasTransform.rotation = Quaternion.LookRotation(Vector3.Normalize(rep.repCanvasTransform.position - Camera.current.transform.position), Vector3.up);
                    }
                }
            }

            if (Server.instance != null) {
                byte[] msgBytes = message.GetBytes();
                Server.instance.BroadcastMessageExcept(NetworkChannel.Unreliable, msgBytes, userId);
            }
        }
    }

    public class PlayerRepSyncData : NetworkMessageData {
        public long userId;
        public bool isGrounded;
        public SimplifiedTransform[] simplifiedTransforms = new SimplifiedTransform[3];
        public Vector3 rootPosition;
        public SimplifiedHand simplifiedLeftHand;
        public SimplifiedHand simplifiedRightHand;
    }
}
