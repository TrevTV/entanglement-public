﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StressLevelZero;

using Entanglement.Representation;
using Entanglement.Extensions;

namespace Entanglement.Network
{
    public class GripRadiusMessageHandler : NetworkMessageHandler
    {
        public override byte? MessageIndex => BuiltInMessageType.GripRadius;

        public override NetworkMessage CreateMessage(NetworkMessageData data)
        {
            if (!(data is GripRadiusMessageData) || data == null)
                throw new Exception("Provided connection data was not of type GripRadiusMessageData or was null!");

            NetworkMessage message = new NetworkMessage();
            GripRadiusMessageData radiusData = data as GripRadiusMessageData;

            message.messageType = (byte)BuiltInMessageType.GripRadius;

            message.messageData = new byte[sizeof(ushort) + sizeof(byte) * 2];

            int index = 0;
            message.messageData = message.messageData.AddBytes(BitConverter.GetBytes(DiscordIntegration.GetShortId(radiusData.userId)), ref index);

            message.messageData[index++] = (byte)radiusData.hand;

            message.messageData[index++] = (byte)(radiusData.radius * 255f);

            return message;
        }

        public override void HandleMessage(NetworkMessage message, long sender)
        {
            if (message.messageData.Length <= 0)
                throw new IndexOutOfRangeException();

            if (SceneLoader.loading)
                return;

            int index = 0;
            // User
            long userId = DiscordIntegration.GetLongId(BitConverter.ToUInt16(message.messageData, index));
            index += sizeof(ushort);

            if (PlayerRepresentation.representations.ContainsKey(userId))
            {
                PlayerRepresentation rep = PlayerRepresentation.representations[userId];

                if (rep.repFord)
                {
                    Handedness hand = (Handedness)message.messageData[index];
                    index += sizeof(byte);

                    float radius = ((float)message.messageData[index]) / 255f;

                    rep.UpdatePoseRadius(hand, radius);
                }
            }

            if (Server.instance != null)
            {
                byte[] msgBytes = message.GetBytes();
                Server.instance.BroadcastMessageExcept(NetworkChannel.Reliable, msgBytes, userId);
            }
        }
    }

    public class GripRadiusMessageData : NetworkMessageData
    {
        public long userId;
        public Handedness hand;
        public float radius;
    }
}
