﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StressLevelZero;

using Entanglement.Representation;
using Entanglement.Extensions;

namespace Entanglement.Network
{
    public class HandPoseChangeMessageHandler : NetworkMessageHandler
    {
        public override byte? MessageIndex => BuiltInMessageType.HandPose;

        public override NetworkMessage CreateMessage(NetworkMessageData data)
        {
            if (!(data is HandPoseChangeMessageData) || data == null)
                throw new Exception("Provided connection data was not of type HandPoseChangeMessageData or was null!");

            NetworkMessage message = new NetworkMessage();
            HandPoseChangeMessageData poseData = data as HandPoseChangeMessageData;

            message.messageType = (byte)BuiltInMessageType.HandPose;

            message.messageData = new byte[sizeof(byte) + sizeof(ushort) * 2];

            int index = 0;
            message.messageData = message.messageData.AddBytes(BitConverter.GetBytes(DiscordIntegration.GetShortId(poseData.userId)), ref index);

            message.messageData[index++] = (byte)poseData.hand;

            byte[] poseIndex = BitConverter.GetBytes(poseData.poseIndex);
            for (int i = 0; i < sizeof(ushort); i++)
                message.messageData[index++] = poseIndex[i];

            return message;
        }

        public override void HandleMessage(NetworkMessage message, long sender)
        {
            if (message.messageData.Length <= 0)
                throw new IndexOutOfRangeException();

            if (SceneLoader.loading)
                return;

            int index = 0;
            long userId = DiscordIntegration.GetLongId(BitConverter.ToUInt16(message.messageData, index));
            index += sizeof(ushort);

            if (PlayerRepresentation.representations.ContainsKey(userId))
            {
                PlayerRepresentation rep = PlayerRepresentation.representations[userId];

                if (rep.repFord) {
                    Handedness hand = (Handedness)message.messageData[index];
                    index += sizeof(byte);

                    int poseIndex = BitConverter.ToUInt16(message.messageData, index);
                    index += sizeof(ushort);

                    rep.UpdatePose(hand, poseIndex);
                }
            }

            if (Server.instance != null)
            {
                byte[] msgBytes = message.GetBytes();
                Server.instance.BroadcastMessageExcept(NetworkChannel.Reliable, msgBytes, userId);
            }
        }
    }

    public class HandPoseChangeMessageData : NetworkMessageData
    {
        public long userId;
        public Handedness hand;
        public ushort poseIndex;
    }
}
