﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MelonLoader;

namespace Entanglement.Network {
    // This is a WIP feature so it isn't gonna be registered for now
    public class TransactionWorkMessageHandler : NetworkMessageHandler {
        public override NetworkMessage CreateMessage(NetworkMessageData data) {
            if (!(data is TransactionWorkMessageData) || data == null)
                throw new Exception("Provided connection data was not of type TransactionWorkMessageData or was null!");

            NetworkMessage message = new NetworkMessage();
            TransactionWorkMessageData transmitData = data as TransactionWorkMessageData;

            message.messageType = (byte)BuiltInMessageType.TransactionWork;

            List<byte> bytes = new List<byte>();

            bytes.Add((byte)transmitData.name.Length);
            bytes.AddRange(Encoding.UTF8.GetBytes(transmitData.name));
            bytes.AddRange(BitConverter.GetBytes((uint)transmitData.data.Length));
            bytes.AddRange(transmitData.data);

            message.messageData = bytes.ToArray();

            return message;
        }

        public override void HandleMessage(NetworkMessage message, long sender) {
            if (message.messageData.Length <= 0)
                throw new IndexOutOfRangeException();

            TransactionWorkMessageData data = new TransactionWorkMessageData();

            byte nameLen = message.messageData[0];
            MelonLogger.Msg(nameLen);

            string fileName = Encoding.UTF8.GetString(message.messageData, 1, nameLen);
            MelonLogger.Msg(fileName);

            uint dataLen = BitConverter.ToUInt32(message.messageData, nameLen + 1);
            MelonLogger.Msg(dataLen);

            uint dataOffset = (uint)nameLen + 1 + sizeof(uint);
            byte[] recvData = new byte[dataLen];
            for (int r = 0; r < dataLen; r++)
                recvData[r] = message.messageData[r + dataOffset];
        }
    }

    public class TransactionWorkMessageData : NetworkMessageData {
        public uint read;
        public byte[] data;
        public string name;
    }
}
