﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MelonLoader;

namespace Entanglement.Network
{
    public class DisconnectMessageHandler : NetworkMessageHandler {
        public override byte? MessageIndex => BuiltInMessageType.Disconnect;

        public override NetworkMessage CreateMessage(NetworkMessageData data) {
            if (!(data is DisconnectMessageData) || data == null)
                throw new Exception("Provided connection data was not of type DisconnectMessageData or was null!");

            NetworkMessage message = new NetworkMessage();
            DisconnectMessageData disconData = data as DisconnectMessageData;

            message.messageType = (byte)BuiltInMessageType.Disconnect;
            message.messageData = new byte[1 + disconData.additionalReason.Length];

            message.messageData[0] = disconData.disconnectReason;
            int index = 1;
            foreach (byte b in Encoding.ASCII.GetBytes(disconData.additionalReason))
                message.messageData[index] = b;

            return message;
        }
        
        // Disconnect messages are only handled by clients
        public override void HandleMessage(NetworkMessage message, long sender) {
            if (message.messageData.Length <= 0)
                throw new IndexOutOfRangeException();

            Client.instance?.DisconnectFromServer();

            byte reason = message.messageData[0];
            string additionalReason = "";

            for (int b = 1; b < message.messageData.Length; b++)
                additionalReason += Encoding.ASCII.GetChars(message.messageData, b, 1);

            MelonLogger.Msg($"You were disconnected for reason {Enum.GetName(typeof(DisconnectReason), reason)}");

            if (additionalReason != string.Empty)
                MelonLogger.Msg($"Additional reason: {additionalReason}");
        }
    }

    public enum DisconnectReason : byte {
        Unknown = 0,
        
        ServerFull   = 19,
        ServerClosed = 20,
        
        Kicked = 50,
        Banned = 51,
        
        OutdatedClient = 100,
        OutdatedServer = 101,
    }

    public class DisconnectMessageData : NetworkMessageData {
        public byte disconnectReason = (byte)DisconnectReason.Unknown;
        public string additionalReason = "";
    }
}
