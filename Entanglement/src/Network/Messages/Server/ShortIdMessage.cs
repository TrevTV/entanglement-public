﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using Entanglement.Extensions;

using MelonLoader;

namespace Entanglement.Network
{
    public class ShortIdMessageHandler : NetworkMessageHandler
    {
        public override byte? MessageIndex => BuiltInMessageType.ShortId;

        public override NetworkMessage CreateMessage(NetworkMessageData data)
        {
            if (!(data is ShortIdMessageData) || data == null)
                throw new Exception("Provided connection data was not of type ShortIdMessageData or was null!");

            NetworkMessage message = new NetworkMessage();
            ShortIdMessageData idData = data as ShortIdMessageData;

            message.messageType = (byte)BuiltInMessageType.ShortId;
            message.messageData = new byte[sizeof(long) + sizeof(ushort)];

            int index = 0;
            message.messageData = message.messageData.AddBytes(BitConverter.GetBytes(idData.userId), ref index);

            message.messageData = message.messageData.AddBytes(BitConverter.GetBytes(idData.shortId), ref index);

            return message;
        }

        public override void HandleMessage(NetworkMessage message, long sender)
        {
            if (message.messageData.Length <= 0)
                throw new IndexOutOfRangeException();

            int index = 0;
            long userId = BitConverter.ToInt64(message.messageData, index);
            index += sizeof(long);

            ushort shortId = BitConverter.ToUInt16(message.messageData, index);

            if (userId == DiscordIntegration.currentUser.Id)
                DiscordIntegration.localShortId = shortId;
            DiscordIntegration.RegisterUser(userId, shortId);
        }
    }

    public class ShortIdMessageData : NetworkMessageData
    {
        public long userId;
        public ushort shortId;
    }
}
