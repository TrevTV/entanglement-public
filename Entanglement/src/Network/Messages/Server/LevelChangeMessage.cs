﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StressLevelZero;

namespace Entanglement.Network {
    public class LevelChangeMessageHandler : NetworkMessageHandler {
        public override byte? MessageIndex => BuiltInMessageType.LevelChange;

        public override NetworkMessage CreateMessage(NetworkMessageData data) {
            if (!(data is LevelChangeMessageData) || data == null)
                throw new Exception("Provided connection data was not of type LevelChangeMessageData or was null!");
            
            NetworkMessage message = new NetworkMessage();
            LevelChangeMessageData levelData = data as LevelChangeMessageData;

            message.messageType = (byte)BuiltInMessageType.LevelChange;
            message.messageData = new byte[] { levelData.sceneIndex };

            return message;
        }

        public override void HandleMessage(NetworkMessage message, long sender) {
            if (message.messageData.Length <= 0)
                throw new IndexOutOfRangeException();

            byte index = message.messageData[0];

            if (index == Client.instance.currentScene)
                return;

            StressLevelZero.Utilities.BoneworksSceneManager.LoadScene(index); // The scene loader only seems to work with an index, but at least it has less weight on the network
        }
    }

    public class LevelChangeMessageData : NetworkMessageData {
        public byte sceneIndex;
    }
}
