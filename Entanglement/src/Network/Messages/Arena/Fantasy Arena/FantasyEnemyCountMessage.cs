﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StressLevelZero.Arena;

using Entanglement.Patching;

using MelonLoader;

namespace Entanglement.Network
{
    public class FantasyEnemyCountMessageHandler : NetworkMessageHandler
    {
        public override byte? MessageIndex => BuiltInMessageType.FantasyCount;

        public override NetworkMessage CreateMessage(NetworkMessageData data)
        {
            if (!(data is FantasyEnemyCountMessageData) || data == null)
                throw new Exception("Provided connection data was not of type FantasyEnemyCountMessageData or was null!");

            NetworkMessage message = new NetworkMessage();
            FantasyEnemyCountMessageData countData = data as FantasyEnemyCountMessageData;

            message.messageType = (byte)BuiltInMessageType.FantasyCount;

            message.messageData = new byte[] { Convert.ToByte(countData.isLow) };

            return message;
        }

        public override void HandleMessage(NetworkMessage message, long sender)
        {
            if (message.messageData.Length <= 0)
                throw new IndexOutOfRangeException();

            bool isLow = Convert.ToBoolean(message.messageData[0]);

            MelonCoroutines.Start(WaitUntilLoaded(isLow));

            if (Server.instance != null) {
                byte[] msgBytes = message.GetBytes();
                Server.instance.BroadcastMessageExcept(NetworkChannel.Reliable, msgBytes, sender);
            }
        }

        public IEnumerator WaitUntilLoaded(bool isLow) {
            while (SceneLoader.loading)
                yield return null;

            Arena_GameManager instance = Arena_GameManager.instance;
            if (!instance) yield break;

            instance.arenaChallengeUI.SetEnemyCount(isLow);
        }
    }

    public class FantasyEnemyCountMessageData : NetworkMessageData {
        public bool isLow = true;
    }
}
