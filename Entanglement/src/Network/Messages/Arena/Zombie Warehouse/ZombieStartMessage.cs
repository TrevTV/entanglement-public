﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

using Entanglement.Patching;

namespace Entanglement.Network
{
    public class ZombieStartMessageHandler : NetworkMessageHandler
    {
        public override byte? MessageIndex => BuiltInMessageType.ZombieStart;

        public override NetworkMessage CreateMessage(NetworkMessageData data)
        {
            if (!(data is ZombieStartMessageData) || data == null)
                throw new Exception("Provided connection data was not of type ZombieStartMessageData or was null!");

            NetworkMessage message = new NetworkMessage();

            message.messageType = (byte)BuiltInMessageType.ZombieStart;
            message.messageData = new byte[0];

            return message;
        }

        public override void HandleMessage(NetworkMessage message, long sender) {
            if (SceneLoader.loading)
                return;

            Zombie_GameControl instance = Zombie_GameControl.instance;
            if (instance) {
                ZombieMode_Settings.m_invalidSettings = true;
                instance.StartSelectedMode();
                ZombieMode_Settings.m_invalidSettings = false;
                instance.uiGameDisplayPageObj.SetActive(true);
                instance.uiSelectPageObj.SetActive(false);
                Transform parent = instance.uiGameDisplayPageObj.transform.parent;
                parent.Find("LoadoutPage").gameObject.SetActive(false);
                parent.Find("CustomModePage").gameObject.SetActive(false);
                parent.Find("DifficultySelectPage").gameObject.SetActive(false);
            }

            if (Server.instance != null) {
                byte[] msgBytes = message.GetBytes();
                Server.instance.BroadcastMessageExcept(NetworkChannel.Reliable, msgBytes, sender);
            }
        }
    }

    public class ZombieStartMessageData : NetworkMessageData {
    }
}
