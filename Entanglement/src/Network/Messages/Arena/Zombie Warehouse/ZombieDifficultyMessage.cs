﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entanglement.Patching;

namespace Entanglement.Network
{
    public class ZombieDifficultyMessageHandler : NetworkMessageHandler
    {
        public override byte? MessageIndex => BuiltInMessageType.ZombieDiff;

        public override NetworkMessage CreateMessage(NetworkMessageData data)
        {
            if (!(data is ZombieDifficultyMessageData) || data == null)
                throw new Exception("Provided connection data was not of type ZombieDifficultyMessageData or was null!");

            NetworkMessage message = new NetworkMessage();
            ZombieDifficultyMessageData diffData = data as ZombieDifficultyMessageData;

            message.messageType = (byte)BuiltInMessageType.ZombieDiff;

            message.messageData = new byte[] { (byte)diffData.difficulty };

            return message;
        }

        public override void HandleMessage(NetworkMessage message, long sender)
        {
            if (message.messageData.Length <= 0)
                throw new IndexOutOfRangeException();

            if (SceneLoader.loading)
                return;

            Zombie_GameControl instance = Zombie_GameControl.instance;
            if (instance) {
                Zombie_GameControl.Difficulty difficulty = (Zombie_GameControl.Difficulty)message.messageData[0];
                instance.SetDifficulty(difficulty);
            }

            if (Server.instance != null) {
                byte[] msgBytes = message.GetBytes();
                Server.instance.BroadcastMessageExcept(NetworkChannel.Reliable, msgBytes, sender);
            }
        }
    }

    public class ZombieDifficultyMessageData : NetworkMessageData {
        public Zombie_GameControl.Difficulty difficulty;
    }
}
