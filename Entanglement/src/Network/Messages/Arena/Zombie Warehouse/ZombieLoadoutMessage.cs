﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entanglement.Patching;

namespace Entanglement.Network
{
    public class ZombieLoadoutMessageHandler : NetworkMessageHandler
    {
        public override byte? MessageIndex => BuiltInMessageType.ZombieLoadout;

        public override NetworkMessage CreateMessage(NetworkMessageData data)
        {
            if (!(data is ZombieLoadoutMessageData) || data == null)
                throw new Exception("Provided connection data was not of type ZombieLoadoutMessageData or was null!");

            NetworkMessage message = new NetworkMessage();
            ZombieLoadoutMessageData loadoutData = data as ZombieLoadoutMessageData;

            message.messageType = (byte)BuiltInMessageType.ZombieLoadout;

            message.messageData = new byte[] { loadoutData.loadIndex };

            return message;
        }

        public override void HandleMessage(NetworkMessage message, long sender)
        {
            if (message.messageData.Length <= 0)
                throw new IndexOutOfRangeException();

            if (SceneLoader.loading)
                return;

            Zombie_GameControl instance = Zombie_GameControl.instance;
            if (instance) {
                byte loadIndex = message.messageData[0];
                ZombieMode_Settings.m_invalidSettings = true;
                instance.ToggleLoadout(loadIndex);
            }

            if (Server.instance != null) {
                byte[] msgBytes = message.GetBytes();
                Server.instance.BroadcastMessageExcept(NetworkChannel.Reliable, msgBytes, sender);
            }
        }
    }

    public class ZombieLoadoutMessageData : NetworkMessageData {
        public byte loadIndex;
    }
}
