﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entanglement.Patching;

namespace Entanglement.Network
{
    public class ZombieModeMessageHandler : NetworkMessageHandler {
        public override byte? MessageIndex => BuiltInMessageType.ZombieMode;

        public override NetworkMessage CreateMessage(NetworkMessageData data) {
            if (!(data is ZombieModeMessageData) || data == null)
                throw new Exception("Provided connection data was not of type ZombieModeMessageData or was null!");

            NetworkMessage message = new NetworkMessage();
            ZombieModeMessageData modeData = data as ZombieModeMessageData;

            message.messageType = (byte)BuiltInMessageType.ZombieMode;

            message.messageData = new byte[] { modeData.mode };

            return message;
        }

        public override void HandleMessage(NetworkMessage message, long sender) {
            if (message.messageData.Length <= 0)
                throw new IndexOutOfRangeException();

            if (SceneLoader.loading)
                return;

            Zombie_GameControl instance = Zombie_GameControl.instance;
            if (instance) {
                byte mode = message.messageData[0];
                ZombieMode_Settings.m_invalidSettings = true;
                instance.SetGameMode(mode);
            }

            if (Server.instance != null) {
                byte[] msgBytes = message.GetBytes();
                Server.instance.BroadcastMessageExcept(NetworkChannel.Reliable, msgBytes, sender);
            }
        }
    }

    public class ZombieModeMessageData : NetworkMessageData {
        public byte mode;
    }
}
