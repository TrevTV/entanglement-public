﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entanglement.Data;
using Entanglement.Extensions;
using Entanglement.Objects;

using UnityEngine;

#if DEBUG
using MelonLoader;
#endif

namespace Entanglement.Network
{
    public class TransformCreateMessageHandler : NetworkMessageHandler
    {
        public override byte? MessageIndex => BuiltInMessageType.TransformCreate;

        public override NetworkMessage CreateMessage(NetworkMessageData data)
        {
            if (!(data is TransformCreateMessageData) || data == null)
                throw new Exception("Provided connection data was not of type TransformCreateMessageData or was null!");

            NetworkMessage message = new NetworkMessage();
            TransformCreateMessageData createData = data as TransformCreateMessageData;

            message.messageType = (byte)BuiltInMessageType.TransformCreate;

            byte[] utf8 = Encoding.UTF8.GetBytes(createData.objectPath);
            
            message.messageData = new byte[sizeof(ushort) * 3 + sizeof(short) + sizeof(float) + sizeof(byte) + utf8.Length];

            int index = 0;
            message.messageData = message.messageData.AddBytes(BitConverter.GetBytes(DiscordIntegration.GetShortId(createData.ownerId)), ref index);

            message.messageData = message.messageData.AddBytes(BitConverter.GetBytes(createData.objectId), ref index);

            message.messageData = message.messageData.AddBytes(BitConverter.GetBytes(createData.callbackIndex), ref index);

            message.messageData = message.messageData.AddBytes(BitConverter.GetBytes(createData.spawnIndex), ref index);

            message.messageData = message.messageData.AddBytes(BitConverter.GetBytes(createData.spawnTime), ref index);

            message.messageData[index++] = Convert.ToByte(createData.enqueueOwner);

            message.messageData = message.messageData.AddBytes(utf8, ref index);

            return message;
        }

        public override void HandleMessage(NetworkMessage message, long sender)
        {
            if (message.messageData.Length <= 0)
                throw new IndexOutOfRangeException();

            if (SceneLoader.loading)
                return;

            int index = 0;
            long ownerId = DiscordIntegration.GetLongId(BitConverter.ToUInt16(message.messageData, index));
            index += sizeof(ushort);

            ushort objectId = 0;
            ushort callbackIndex = 0;

            if (Server.instance != null)
            {
                objectId = ObjectSync.lastId;
                objectId += 1;
                ObjectSync.lastId = objectId;
                message.messageData = message.messageData.AddBytes(BitConverter.GetBytes(objectId), index);

                index += sizeof(ushort);

                callbackIndex = BitConverter.ToUInt16(message.messageData, index);
                index += sizeof(ushort);
            }
            else {
                objectId = BitConverter.ToUInt16(message.messageData, index);
                ObjectSync.lastId = objectId;
                index += sizeof(ushort) * 2;
            }

            short spawnIndex = BitConverter.ToInt16(message.messageData, index);
            index += sizeof(short);

            float spawnTime = BitConverter.ToSingle(message.messageData, index);
            index += sizeof(float);

            bool enqueueOwner = Convert.ToBoolean(message.messageData[index++]);

            byte[] pathBytes = new byte[message.messageData.Length - index];
            for (int i = 0; i < pathBytes.Length; i++)
                pathBytes[i] = message.messageData[index++];

            string objectPath = Encoding.UTF8.GetString(pathBytes);

            Transform objectTransform = objectPath.GetFromFullPath(spawnIndex, spawnTime);
            bool destroySync = false;
            if (objectTransform) {
#if DEBUG
                MelonLogger.Msg($"Retrieved object from path {objectPath}!");
#endif

                SyncedTransform existingSync = SyncedTransform.cache.GetOrAdd(objectTransform.gameObject);
                if (existingSync) {
#if DEBUG
                    MelonLogger.Msg("Object is already synced! Don't freeze it!");
#endif
                    ObjectSync.MoveSyncable(existingSync, objectId);
                    existingSync.ClearOwner();
                    existingSync.TrySetStale(ownerId);

                    if (enqueueOwner) {
                        existingSync.EnqueueOwner(ownerId);
                    }
                }
                else {
#if DEBUG
                    MelonLogger.Msg($"Creating sync object!");
#endif

                    Syncable syncable = SyncedTransform.CreateSync(ownerId, ComponentCacheExtensions.m_RigidbodyCache.GetOrAdd(objectTransform.gameObject), objectId);

                    if (enqueueOwner) {
                        syncable.EnqueueOwner(ownerId);
                    }
                }

                if (!objectTransform.gameObject.activeInHierarchy)
                    objectTransform.ForceActivate();
            }
            else {
#if DEBUG
                MelonLogger.Warning($"Failed to retrieve object from path {objectPath}!");
#endif
            }

            ObjectSync.lastId = objectId;

            if (Server.instance != null) {
                // Send callback to owner
                IDCallbackMessageData idCallback = new IDCallbackMessageData()
                {
                    objectIndex = callbackIndex,
                    newId = objectId,
                    destroySync = destroySync,
                };

                NetworkMessage callbackMessage = NetworkMessage.CreateMessage((byte)BuiltInMessageType.IDCallback, idCallback);
                Server.instance.SendMessage(ownerId, NetworkChannel.Object, callbackMessage.GetBytes());

                // Send sync create to clients
                byte[] msgBytes = message.GetBytes();
                Server.instance.BroadcastMessageExcept(NetworkChannel.Object, msgBytes, ownerId);
            }
        }
    }

    public class TransformCreateMessageData : NetworkMessageData
    {
        public long ownerId;
        public ushort objectId;
        public ushort callbackIndex;
        public short spawnIndex = -1; // Used as an identifier to work-around different uuids
        public float spawnTime = -1f;
        public bool enqueueOwner = true;
        public string objectPath;
    }
}
