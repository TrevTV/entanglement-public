﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entanglement.Data;
using Entanglement.Extensions;
using Entanglement.Objects;
using Entanglement.Patching;

using StressLevelZero.Pool;
using StressLevelZero.Interaction;

using UnityEngine;

namespace Entanglement.Network
{
    public class MagazinePlugMessageHandler : NetworkMessageHandler
    {
        public override byte? MessageIndex => BuiltInMessageType.MagazinePlug;

        public override NetworkMessage CreateMessage(NetworkMessageData data)
        {
            if (!(data is MagazinePlugMessageData) || data == null)
                throw new Exception("Provided connection data was not of type MagazinePlugMessageData or was null!");

            NetworkMessage message = new NetworkMessage();
            MagazinePlugMessageData plugData = data as MagazinePlugMessageData;

            message.messageType = (byte)BuiltInMessageType.MagazinePlug;

            message.messageData = new byte[sizeof(ushort) * 2 + sizeof(bool)];

            int index = 0;
            message.messageData = message.messageData.AddBytes(BitConverter.GetBytes(plugData.magId), ref index);

            message.messageData = message.messageData.AddBytes(BitConverter.GetBytes(plugData.gunId), ref index);

            message.messageData[index++] = Convert.ToByte(plugData.isInsert);

            return message;
        }

        public override void HandleMessage(NetworkMessage message, long sender)
        {
#if DEBUG
            MelonLoader.MelonLogger.Msg("Received mag sync message!");
#endif

            if (message.messageData.Length <= 0)
                throw new IndexOutOfRangeException();

            if (SceneLoader.loading)
                return;

#if DEBUG
            MelonLoader.MelonLogger.Msg("Got past load and length!");
#endif

            int index = 0;
            ushort magId = BitConverter.ToUInt16(message.messageData, index);
            index += sizeof(ushort);

            ushort gunId = BitConverter.ToUInt16(message.messageData, index);
            index += sizeof(ushort);

            if (ObjectSync.TryGetSyncable(magId, out Syncable magazine) && ObjectSync.TryGetSyncable(gunId, out Syncable gun)) {
#if DEBUG
                MelonLoader.MelonLogger.Msg("Got syncables!");
#endif

                SyncedTransform syncMag = magazine.TryCast<SyncedTransform>();
                SyncedTransform syncGun = gun.TryCast<SyncedTransform>();

                if (syncMag && syncGun) {

                    if (syncMag._CachedPlug && syncGun._CachedGun) {

                        bool isInsert = Convert.ToBoolean(message.messageData[index++]);

                        MagazineSocket magSocket = syncGun._CachedGun.magazineSocket;

                        if (isInsert) {
                            syncMag._CachedPlug.InsertPlug(magSocket);

#if DEBUG
                            MelonLoader.MelonLogger.Msg("Trying to insert a magazine!");
#endif
                        }
                        else {
                            syncMag._CachedPlug.ForceEject();

#if DEBUG
                            MelonLoader.MelonLogger.Msg("Trying to eject a magazine!");
#endif
                        }
                    }
#if DEBUG
                    else
                        MelonLoader.MelonLogger.Msg($"No cached plug or gun? Object names are mag {syncMag.name} and gun {syncGun.name}.");
#endif
                }
#if DEBUG
                else {
                    MelonLoader.MelonLogger.Msg("Failed to cast syncables to SyncedTransform!");
                }
#endif
            }

            if (Server.instance != null) {
                byte[] msgBytes = message.GetBytes();
                Server.instance.BroadcastMessageExcept(NetworkChannel.Reliable, msgBytes, sender);
            }
        }
    }

    public class MagazinePlugMessageData : NetworkMessageData
    {
        public ushort magId;
        public ushort gunId;
        public bool isInsert = true;
    }
}
