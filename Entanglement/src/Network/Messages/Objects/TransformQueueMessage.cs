﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entanglement.Data;
using Entanglement.Extensions;
using Entanglement.Objects;

namespace Entanglement.Network
{
    public class TransformQueueMessageHandler : NetworkMessageHandler
    {
        public override byte? MessageIndex => BuiltInMessageType.TransformQueue;

        public override NetworkMessage CreateMessage(NetworkMessageData data)
        {
            if (!(data is TransformQueueMessageData) || data == null)
                throw new Exception("Provided connection data was not of type TransformQueueMessageData or was null!");

            NetworkMessage message = new NetworkMessage();
            TransformQueueMessageData queueData = data as TransformQueueMessageData;

            message.messageType = (byte)BuiltInMessageType.TransformQueue;

            message.messageData = new byte[sizeof(ushort) * 2 + sizeof(byte)];

            int index = 0;
            message.messageData = message.messageData.AddBytes(BitConverter.GetBytes(DiscordIntegration.GetShortId(queueData.userId)), ref index);

            message.messageData = message.messageData.AddBytes(BitConverter.GetBytes(queueData.objectId), ref index);

            message.messageData[index++] = Convert.ToByte(queueData.isAdd);

            return message;
        }

        public override void HandleMessage(NetworkMessage message, long sender)
        {
            if (message.messageData.Length <= 0)
                throw new IndexOutOfRangeException();

            if (SceneLoader.loading)
                return;

            int index = 0;
            long userId = DiscordIntegration.GetLongId(BitConverter.ToUInt16(message.messageData, index));
            index += sizeof(ushort);

            ushort objectId = BitConverter.ToUInt16(message.messageData, index);
            index += sizeof(ushort);

            if (ObjectSync.TryGetSyncable(objectId, out Syncable syncable)) {
                bool isAdd = Convert.ToBoolean(message.messageData[index++]);

                // Try to enqueue the user
                if (isAdd) {
                    syncable.EnqueueOwner(userId);
                }
                // Remove the user from the queue
                else {
                    syncable.DequeueOwner(userId);
                }
            }

            if (Server.instance != null)
            {
                byte[] msgBytes = message.GetBytes();
                Server.instance.BroadcastMessage(NetworkChannel.Object, msgBytes);
            }
        }
    }

    public class TransformQueueMessageData : NetworkMessageData
    {
        public long userId;
        public ushort objectId;
        public bool isAdd;
    }
}
