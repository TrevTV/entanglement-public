﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entanglement.Data;
using Entanglement.Extensions;
using Entanglement.Objects;

using StressLevelZero.Pool;

using UnityEngine;

namespace Entanglement.Network
{
    public class TransformSyncMessageHandler : NetworkMessageHandler 
    {
        public override byte? MessageIndex => BuiltInMessageType.TransformSync;

        public override NetworkMessage CreateMessage(NetworkMessageData data)
        {
            if (!(data is TransformSyncMessageData) || data == null)
                throw new Exception("Provided connection data was not of type TransformSyncMessageData or was null!");

            NetworkMessage message = new NetworkMessage();
            TransformSyncMessageData transformSync = data as TransformSyncMessageData;

            message.messageType = (byte)BuiltInMessageType.TransformSync;

            message.messageData = new byte[sizeof(ushort) * 2 + SimplifiedTransform.size];

            int index = 0;
            message.messageData = message.messageData.AddBytes(BitConverter.GetBytes(transformSync.objectId), ref index);

            message.messageData = message.messageData.AddBytes(transformSync.simplifiedTransform.GetBytes(), ref index);

            return message;
        }

        public override void HandleMessage(NetworkMessage message, long sender)
        {
            if (message.messageData.Length <= 0)
                throw new IndexOutOfRangeException();

            if (SceneLoader.loading)
                return;

            int index = 0;
            ushort objectId = BitConverter.ToUInt16(message.messageData, index);
            index += sizeof(ushort);

            if (ObjectSync.TryGetSyncable(objectId, out Syncable syncable)) {
                if (syncable is SyncedTransform) {
                    SyncedTransform syncObj = syncable.Cast<SyncedTransform>();

                    SimplifiedTransform simpleTransform = SimplifiedTransform.FromBytes(message.messageData.ToList().GetRange(index, SimplifiedTransform.size).ToArray());
                    syncObj.ApplyTransform(simpleTransform);

                    GameObject go = syncObj.gameObject;

                    if (!go.activeInHierarchy)
                        go.transform.ForceActivate();
                }
            }

            if (Server.instance != null) {
                byte[] msgBytes = message.GetBytes();
                Server.instance.BroadcastMessageExcept(NetworkChannel.Unreliable, msgBytes, sender);
            }
        }
    }

    public class TransformSyncMessageData : NetworkMessageData {
        public ushort objectId;
        public SimplifiedTransform simplifiedTransform;
    }
}
