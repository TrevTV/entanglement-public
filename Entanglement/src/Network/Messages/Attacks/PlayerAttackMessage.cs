﻿using System;

using Entanglement.Data;
using Entanglement.Representation;
using Entanglement.Extensions;

using StressLevelZero.Combat;

namespace Entanglement.Network
{
    public class PlayerAttackMessageHandler : NetworkMessageHandler
    {
        public override byte? MessageIndex => BuiltInMessageType.PlayerAttack;


        public override NetworkMessage CreateMessage(NetworkMessageData data)
        {
            if (!(data is PlayerAttackMessageData) || data == null)
                throw new Exception("Provided connection data was not of type PlayerAttackMessageData or was null!");

            NetworkMessage message = new NetworkMessage();
            PlayerAttackMessageData attackData = data as PlayerAttackMessageData;

            message.messageType = (byte)BuiltInMessageType.PlayerAttack;

            message.messageData = new byte[sizeof(byte) + sizeof(ushort)];

            int index = 0;
            message.messageData[index++] = (byte)attackData.attackType;

            message.messageData = message.messageData.AddBytes(BitConverter.GetBytes((ushort)(attackData.attackDamage * 10000f)), ref index);

            return message;
        }

        public override void HandleMessage(NetworkMessage message, long sender)
        {
            if (message.messageData.Length <= 0)
                throw new IndexOutOfRangeException();

            if (SceneLoader.loading)
                return;

            int index = 0;
            AttackType attackType = (AttackType)message.messageData[index++];

            float attackDamage = BitConverter.ToUInt16(message.messageData, index) / 10000f;

            PlayerScripts.playerHealth.TAKEDAMAGE(attackDamage);

            // Play Sound
            if (PlayerRepresentation.representations.ContainsKey(sender))
            {
                PlayerRepresentation rep = PlayerRepresentation.representations[sender];
                switch (attackType) {
                    case AttackType.Stabbing:
                        rep.repStabSFX.GunShot();
                        break;
                }
            }
        }
    }

    public class PlayerAttackMessageData : NetworkMessageData {
        public AttackType attackType = AttackType.None;
        public float attackDamage;
    }
}
