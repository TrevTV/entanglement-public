﻿using System;
using System.Net;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using System.IO;

using Discord;

using MelonLoader;

using UnityEngine;

using Entanglement.UI;
using Entanglement.Extensions;

namespace Entanglement.Network
{
    public enum VoiceStatus {
        Disabled,
        Enabled,
    }

    public static class DiscordIntegration {
        public static Discord.Discord discord;

        public static ActivityManager activityManager;
        public static LobbyManager lobbyManager;
        public static UserManager userManager;
        public static ImageManager imageManager;
        public static VoiceManager voiceManager;

        public static Activity activity;
        public static Lobby lobby;

        public static User currentUser;

        public static bool hasLobby => lobby.Id != 0;
        public static bool isHost => hasLobby && lobby.OwnerId == currentUser.Id;
        public static bool isConnected => hasLobby && lobby.OwnerId != currentUser.Id;

        public static VoiceStatus voiceStatus = VoiceStatus.Disabled;

        public static Dictionary<ushort, long> shortUserIds = new Dictionary<ushort, long>();
        // An ID of 0 is reserved for the host
        public static ushort localShortId = 0;
        public static ushort lastShortId = 1;

        public static long GetLongId(ushort shortId) {
            if (shortId == 0) return lobby.OwnerId;

            return shortUserIds.TryIdx(shortId);
        }

        public static ushort GetShortId(long longId) {
            if (longId == currentUser.Id) return localShortId;
            
            return shortUserIds.FirstOrDefault(o => o.Value == longId).Key;
        }

        public static ushort CreateShortId() => lastShortId++;

        public static void RegisterUser(long userId, ushort shortId) => shortUserIds.Add(shortId, userId);

        public static ushort RegisterUser(long userId) {
            ushort shortId = CreateShortId();
            RegisterUser(userId, shortId);
            return shortId;
        }

        public static void RemoveUser(long userId) => shortUserIds.Remove(GetShortId(userId));

        public static void Initialize() {
            discord = new Discord.Discord(883939020269293618, (System.UInt64)CreateFlags.Default);
            activityManager = discord.GetActivityManager();

            lobbyManager = discord.GetLobbyManager();

            userManager = discord.GetUserManager();

            userManager.OnCurrentUserUpdate += () => {
                currentUser = userManager.GetCurrentUser();
                MelonLogger.Msg($"Current Discord User: {currentUser.Username}");
            };

            imageManager = discord.GetImageManager();

            voiceManager = discord.GetVoiceManager();

            DefaultRichPresence();
        }

        public static void DefaultRichPresence() {
            activity = new Activity()
            {
                Name = "Entanglement",
                Details = "Playing solo",
                Assets = new ActivityAssets() { LargeImage = "boneworks", LargeText = "This user isn't hosting a game!" }
            };

            activity.Instance = false;

            activityManager.UpdateActivity(activity, (x) => { });
        }

        public static void Update() => discord.RunCallbacks();

        public static void Flush() => lobbyManager.FlushNetwork();

        public static void Tick() {
            Update();
            Flush();
        }

        public static void Shutdown() {
            discord.ActivityManagerInstance.ClearActivity((x) => { });
            discord.Dispose();
        }

        public static void UpdateVoice(VoiceStatus status) {
            voiceStatus = status;

            if (!hasLobby) return;

            switch (status) {
                default:
                case VoiceStatus.Disabled:
                    lobbyManager.DisconnectVoice(lobby.Id, (res) => { });
                    break;
                case VoiceStatus.Enabled:
                    lobbyManager.ConnectVoice(lobby.Id, (res) => { });
                    break;
            }
        }
    }
}
