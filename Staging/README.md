# Entanglement Pre-Release

### Warning: This mod is still WIP and is not final yet!

An alternative multiplayer mod for BONEWORKS written with performance and scalability in mind!

This mod uses discord networking and therefore is compatible with both Steam and Oculus clients, enjoy having cross platform multiplayer!

Join our discord server! https://discord.gg/FmRGBgbc3N

Check out the git repo for the source code: https://gitlab.com/zCubed3/entanglement-public

# Changelog
### **v0.0.5 - Current**
- Fixed a problem with lobby capacities

### **v0.0.4**
- Fixed a critical divide by zero error that happened when pausing to the SteamVR menu

### **v0.0.3**
- Optimized the mod for better performance
- Increased stability of physics
